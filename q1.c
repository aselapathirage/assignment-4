#include <stdio.h>
/* Question 1: program to calculate the sum and average of any given 3 integers and print the
output. */

int main(){
    int num1=0,num2=0,num3=0, sum=0;
    printf("Enter number 1: ");
    scanf("%d", &num1);
    printf("Enter number 2: ");
    scanf("%d", &num2);
    printf("Enter number 3: ");
    scanf("%d", &num3);
    /* Calculating the sum */
    sum=num1+num2+num3;
    printf("Sum = %d + %d + %d = %d \n", num1, num2, num3, sum);
    /*Printing the average */
    printf("Average = %.2lf \n", (double)sum/3);

    return 0;
}
