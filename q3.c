#include <stdio.h>
/* Question 3: program to swap two numbers without using a temporary third variable. */

int main(){
    int num1=0, num2=0;
    printf("Enter a number A: ");
    scanf("%d", &num1);
    printf("Enter a number B: ");
    scanf("%d", &num2);
    printf("Number A : %d\tNumber B: %d\n", num1, num2);

    /* Swapping 2 numbers */
    num1=num1-num2;
    num2=num1+num2;
    num1=num2-num1;

    printf("Number A : %d\tNumber B: %d\n", num1, num2);

    return 0;
}
