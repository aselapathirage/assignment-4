#include <stdio.h>
/* Program to display the relationship between profit and ticket price; and the price at which make the highest profit in a movie theater */

/*function to calculate the income*/
int income(int ticketPrice);

/*function to calculate the expenditure*/
int expenditure(int ticketPrice);

/*function to calculate the profit*/
int profit(int ticketPrice);

/*function to calculate the number of attendees*/
int noOfAttendees(int ticketPrice);

int income(int ticketPrice){
    return ticketPrice * noOfAttendees(ticketPrice);
}

int expenditure(int ticketPrice){
    return 500 + (3 * noOfAttendees(ticketPrice));
}

int profit(int ticketPrice){
    return income(ticketPrice) - expenditure(ticketPrice);
}

int noOfAttendees(int ticketPrice){
    return 120-((ticketPrice-15)/5 * 20);
}

int main(){
    int maxProfitTicket=0, maxProf=0;

    /*Printing the relationship table and showing which ticket price makes the highest profit*/
    printf("Relationship between Ticket price and Profit\n\n");
    printf("Ticket Price(Rs)|\tProfit(Rs)\n---------------------------------\n");

    for(int i=5; i<=100; i+=5){
        printf(" %d\t\t\t%d\n", i, profit(i));
        if(maxProf<profit(i)){
            maxProf=profit(i);
            maxProfitTicket=i;
        }
    }

    printf("So, when the Ticket price is Rs.%d, make the highest profit of Rs.%d\n ", maxProfitTicket, profit(maxProfitTicket));

    return 0;
}
