#include <stdio.h>
/* Question 5: program to demonstrate given bitwise operators */
int printb(int);

int main(){
    int A=10,B=15;
    printf("A : %d , B : %d\n\n",A,B);
    /* A&B */
    printf("a. A&B : %d & %d = %d\n", A,B,A&B);
    printf("A  : %d\n",printb(A));
    printf("B  : %d\n",printb(B));
    printf("A&B: %d\n\n",printb(A&B));
    /* A^B */
    printf("b. A^B : %d ^ %d = %d\n", A,B,A^B);
    printf("A  : %d\n",printb(A));
    printf("B  : %d\n",printb(B));
    printf("A^B: %d\n\n",printb(A^B));
    /* ~A */
    printf("c. ~A : %d \n", ~A);
    printf("A  : %d\n",printb(A));
    printf("~A : %d\n\n",printb(~A));
    /* A<<3 */
    printf("d. A<<3 : %d\n", A<<3);
    printf("A   : %d\n",printb(A));
    printf("A<<3: %d\n\n",printb(A<<3));
    /* B>>3 */
    printf("e. B>>3 : %d\n", B>>3);
    printf("B   : %d\n",printb(B));
    printf("B>>3: %d\n\n",printb(B>>3));

    return 0;
}

int printb(int n){
    if(n==0)return 0;
    else return (n%2)+10*printb(n/2);
}
