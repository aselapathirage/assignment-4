#include <stdio.h>
/* Question 4: program to convert temperature given in Celsius to Fahrenheit. You should round
the output value into 2 decimal points. */

int main(){
    double temp=0.0;
    printf("Enter the temperature in Celsius: ");
    scanf("%lf", &temp);

    /* Conversion of temperature to Fahrenheit */
    temp=(temp*((double)9/5))+32;
    printf("Temperature in Fahrenheit: %.2lf\n", temp);
    return 0;
}
