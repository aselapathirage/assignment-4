#include <stdio.h>
/* Question 2: program to find the volume of the cone where the user inputs the height and radius
of the cone. */

int main(){
    double radius=0.0, height=0.0;
    double volume=0.0;
    printf("Enter the radius: ");
    scanf("%lf", &radius);
    printf("Enter the height: ");
    scanf("%lf", &height);

    volume=((double)22/7)*radius*radius*((double)height/3);
    printf("Volume of the cone : %.2lf\n", volume);

    return 0;
}
